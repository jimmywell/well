<?php
/**
 * MailChimp Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'n2t-block-mailchimp-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = 'n2t-block-mailchimp';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}
?>
<div style="background-color: <?php the_field('background_color'); ?>" id="social-block_<?php echo $id; ?>" class="comp social-block mntl-block <?php echo $className; ?>">
    <div id="newsletter-signup-vue_2-0" class="comp newsletter-signup-vue nl-loaded">
        <?php echo do_shortcode('[mc4wp_form id="'.get_field('form').'"]'); ?>
    </div>
    <img class="social-block-image ls-is-cached lazyloaded" src="<?php the_field('background_image'); ?>">
    <div id="social-follow_2-0" class="comp social-follow static-social-follow mntl-block" data-tracking-container="true">
        <div id="social-follow__title_2-0" class="comp social-follow__title mntl-text-block">
            Follow Us
        </div>
        <div id="social-follow__intro_2-0" class="comp social-follow__intro mntl-text-block"></div>
        <ul id="social-follow__list_2-0" class="comp social-follow__list mntl-block">
            <li id="social-follow__item_2-0" class="comp social-follow__item social-follow__btn mntl-block">
                <a href="//www.facebook.com/well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_2-0" class=" social-follow__link mntl-text-link social-follow__link--facebook" data-tracking-container="true">
                    <span id="mntl-text-block_2-0" class="comp is-vishidden mntl-text-block">
                    facebook</span>
                    <svg class="icon icon-facebook ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-facebook"></use>
                    </svg>
                </a>
            </li>
            <li id="social-follow__item_2-0-1" class="comp social-follow__item social-follow__btn mntl-block">
                <a href="//www.pinterest.com/well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_2-0-1" class=" social-follow__link mntl-text-link social-follow__link--pinterest" data-tracking-container="true">
                    <span id="mntl-text-block_2-0-1" class="comp is-vishidden mntl-text-block">
                    pinterest</span>
                    <svg class="icon icon-pinterest ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pinterest"></use>
                    </svg>
                </a>
            </li>
            <li id="social-follow__item_2-0-2" class="comp social-follow__item social-follow__btn mntl-block">
                <a href="//instagram.com/well.songkhoe" target="_blank" rel="noopener" id="social-follow__link_2-0-2" class=" social-follow__link mntl-text-link social-follow__link--instagram" data-tracking-container="true">
                    <span id="mntl-text-block_2-0-2" class="comp is-vishidden mntl-text-block">
                    instagram</span>
                    <svg class="icon icon-instagram ">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-instagram"></use>
                    </svg>
                </a>
            </li>
        </ul>
    </div>
</div>