<?php
    global $wp_query;
    $author = get_queried_object();
    get_header();
?>
<div id="bio-header_1-0" class="comp bio-header mntl-block">
	<figure id="bio-header__image_1-0" class="comp figure-small bio-header__image figure-article mntl-block">
		<div id="figure-article__media_1-0" class="comp figure-article__media mntl-block">
			<div class="img-placeholder" style="padding-bottom:100.0%;">
				<img src="<?php the_field('author_image', 'user_' . $author->ID); ?>" alt="<?php the_author_meta('display_name', $author->ID); ?>" class=" figure-article__image mntl-primary-image ">
			</div>
		</div>
	</figure>
	<div id="bio-header__details_1-0" class="comp bio-header__details mntl-block">
		<h1 id="bio-details__name_1-0" class="comp bio-details__name mntl-text-block">
			<?php the_author_meta('display_name', $author->ID); ?>
		</h1>
		<div id="bio-details__role_1-0" class="comp bio-details__role mntl-block">
			<span id="bio-details__title_1-0" class="comp bio-details__title mntl-text-block">
                <?php the_field('job_title', 'user_' . $author->ID); ?>
            </span>
		</div>
		<div id="mntl-block_1-0" class="comp bio-details__ex-ed mntl-block">
			<h2 id="mntl-text-block_4-0" class="comp bio-details__ex-ed__title mntl-text-block">
				Expertise
			</h2>
			<span id="bio-expertise__items_1-0" class="comp bio-details__ex-ed__items bio-expertise__items mntl-text-block"><?php the_field('expertise', 'user_' . $author->ID); ?></span>
		</div>
		<div id="mntl-block_2-0" class="comp bio-details__ex-ed mntl-block">
			<h2 id="mntl-text-block_5-0" class="comp bio-details__ex-ed__title mntl-text-block">
				Education
			</h2>
			<span id="bio-education__items_1-0" class="comp bio-details__ex-ed__items bio-education__items mntl-text-block"> <?php the_field('education', 'user_' . $author->ID); ?></span>
		</div>
		<div id="social-follow_2-0" class="comp extended social-follow mntl-block">
			<div id="social-follow__intro_2-0" class="comp social-follow__intro mntl-text-block"></div>
			<ul id="social-follow__list_2-0" class="comp social-follow__list mntl-block">
                <?php
                    $socials = get_field('socials', 'user_' . $author->ID);
                    if(!empty($socials)):
                        foreach ($socials as $name => $url):
                ?>
				<li id="social-follow__item_2-0" class="comp social-follow__item social-follow__btn mntl-block">
					<a href="<?php echo $url; ?>" target="_blank" rel="noopener" id="social-follow__link_2-0" class=" social-follow__link mntl-text-link social-follow__link--<?php echo $name; ?>">
						<span id="mntl-text-block_6-0" class="comp is-vishidden mntl-text-block"><?php echo $name; ?></span>
						<svg class="icon icon-<?php echo $name; ?> ">
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-<?php echo $name; ?>"></use>
						</svg>
					</a>
				</li>
                <?php endforeach; endif; ?>
			</ul>
		</div>
	</div>
</div>
<div id="bio-content_1-0" class="comp expert-content bio-content">
	<?php the_field('description', 'user_' . $author->ID); ?>
</div>
<section id="bio-article-list_1-0" class="comp bio-article-list article-list">
	<span class="section-title">More From <?php the_author_meta('display_name', $author->ID); ?></span>
	<div class="loc content section-body">
		<ul id="posts_container" class="comp g g-four-up block-list" data-chunk="24">
			<?php
                while(have_posts()):
                    the_post();
                get_template_part('template/loop/content');
                endwhile;
            ?>
		</ul>
        <?php if($wp_query->max_num_pages > 1): ?>
		<a href="#" data-author="<?php echo get_queried_object_id(); ?>" data-current-page="1" data-total-pages="<?php echo  $wp_query->max_num_pages; ?>" class="btn-link n2t-load-more" aria-label="View More">
			<button class="btn btn-divider btn-dark" id="divider-button">
				<div class="btn-divider-inner">
					<span>View More</span>
					<svg class="icon icon-circle-arrow-down btn-icon">
						<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-circle-arrow-down"></use>
					</svg>
				</div>
			</button>
		</a>
        <?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>