(function ($) {

    $(document).on('click touchend', '.menu-button, .search-button', function(event){
        event.preventDefault();

        $("html").toggleClass("is-noscroll");
        $("body").toggleClass("is-noscroll");
        $(".header-nav-panel").scrollTop(0);
        $(".header").toggleClass("is-fullnav");
        $(".main").toggleClass("is-invisible");
        $(".footer").toggleClass("is-invisible");

        if($(this).hasClass("search-button")){
            $(".header").find(".general-search-input").focus()
        }
    });

    $(document).ready(function () {
        $(window).scroll(function(){
            if($(this).scrollTop() > 0){
                $(".header").addClass("header-condensed");
            } else {
                $(".header").removeClass("header-condensed");
            }
        });

        if($('#search_input').length > 0) {
            $('#search_input').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: n2t.ajaxURL,
                        data: {
                            'action': 'n2t_search_posts',
                            'query': request.term
                        },
                        success: function (data) {
                            response($.map(data, function (v, i) {
                                return {
                                    label: v.post_title,
                                    value: v.guid
                                };
                            }));
                        }
                    });
                },
                select: function (event, ui) {
                    window.location.href = ui.item.value;
                }
            });
        }

        if($("a.n2t-load-more").length > 0) {
            $("a.n2t-load-more").on('click touchend', function (e) {
                e.preventDefault();
                var button = $(this),
                    current_page = button.attr('data-current-page'),
                    current_author = button.attr('data-author'),
                    current_cat = button.attr('data-cat'),
                    total_pages = button.attr('data-total-pages'),
                    container = $("#posts_container");
                if (!button.hasClass('loading')) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: n2t.ajaxURL,
                        data: {
                            'action': 'n2t_get_posts',
                            'current_page': current_page,
                            'cat': current_cat,
                            'author': current_author
                        },
                        beforeSend: function(){
                            button.addClass('loading').css('opacity', 0.5);
                        },
                        success: function (response) {
                            if (response.success == true) {
                                button.removeClass('loading').css('opacity', 1);
                                container.append(response.html);
                            }
                            button.attr('data-current-page', response.current_page);
                            if (response.current_page >= total_pages) {
                                button.hide();
                            }
                        }
                    });
                }
            });
        }
    });
}(jQuery));